# README #

### What is this repository for? ###

This is project for Rest Quest. Server is backend software. Examples are basic implementation of solution of quest.

Tasks to quest can be found in file Tasks.doc


### How do I get set up? ###

To run server locally use command

Windows:

```
gradlew rest-quest-server:bootRun
```

Linux/mac:

```
./gradlew rest-quest-server:bootRun
```


### Who do I talk to? ###

Repo owner or admin: 

* email: ugolnikovroman@gmail.com

* skype: ugolnikovroman


## Using Sonarqube in the project.

First, you need to have Docker installed on your machine.

Run sonarqube in the docker container:

```
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube:5.2
```

After that, run code analize:

```
 gradle sonarqube -Dsonar.host.url="http://[ip_of_running_docker_container]:9000"
```

for example for Docker toolbox on windows/maxOs

```
 gradle sonarqube -Dsonar.host.url="http://192.168.99.100:9000"
```

or for linux

```
gradle sonarqube -Dsonar.host.url="http://localhost:9000"
```

more info:
https://plugins.gradle.org/plugin/org.sonarqube
http://docs.sonarqube.org/display/SONAR/Analyzing+with+SonarQube+Scanner+for+Gradle
