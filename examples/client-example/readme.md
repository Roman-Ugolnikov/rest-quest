
##For test purposes
Run several instances windows

    @echo off
    start "Title1" java -jar rest-quest-client-0.2.0-SNAPSHOT.jar
    start "Title2" java -jar rest-quest-client-0.2.0-SNAPSHOT.jar
    start "Title3" java -jar rest-quest-client-0.2.0-SNAPSHOT.jar
    pause

linux:

    #!/bin/bash
    java -jar rest-quest-client-0.2.0-SNAPSHOT.jar &
    java -jar rest-quest-client-0.2.0-SNAPSHOT.jar &
    java -jar rest-quest-client-0.2.0-SNAPSHOT.jar &

## Configure
to specify remote host (not default localhost) use

    java -jar -Durl='http://localhost:8080/users' rest-quest-client-0.2.0-SNAPSHOT.jar