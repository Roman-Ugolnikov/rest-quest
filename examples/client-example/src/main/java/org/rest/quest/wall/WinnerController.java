package org.rest.quest.wall;

import org.rest.quest.domain.init.wrapper.MessageWrapper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;

/**
 * Created by r.uholnikov on 26.03.2016.
 */
@RestController
public class WinnerController {

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String getWinnerKeyword(@RequestBody MessageWrapper messageWrapper) throws UnknownHostException {
        WinnerResolver.KEYWORD = messageWrapper.getKeyword();
        WinnerResolver.sendWinnerUrl(WinnerResolver.HOST_URL, WinnerResolver.KEYWORD, WinnerResolver.TEAM_ID, true);
        return "success";
    }

    @RequestMapping(value = "/start", method = RequestMethod.GET)
    public String getWinnerKeyword() throws UnknownHostException {
        return "success";
    }

}
