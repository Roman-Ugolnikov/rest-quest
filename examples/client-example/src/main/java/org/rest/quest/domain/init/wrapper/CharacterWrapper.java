package org.rest.quest.domain.init.wrapper;

class CharacterWrapper {
    Character character;
    Long usage;

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public Long getUsage() {
        return usage;
    }

    public void setUsage(Long usage) {
        this.usage = usage;
    }

    public void incrementUsage() {
        this.usage ++;
    }

    public CharacterWrapper(Character character, Long usage) {
        this.character = character;
        this.usage = usage;
    }
}
