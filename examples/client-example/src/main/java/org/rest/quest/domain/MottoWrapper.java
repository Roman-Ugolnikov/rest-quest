package org.rest.quest.domain;

/**
 * @author Roman Uholnikov
 */
public class MottoWrapper {
    int id;
    String motto;
    int weight;

    public MottoWrapper() {
    }

    public MottoWrapper(final int id, final String motto, final int weight) {
        this.id = id;
        this.motto = motto;
        this.weight = weight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(final String motto) {
        this.motto = motto;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(final int weight) {
        this.weight = weight;
    }

}
