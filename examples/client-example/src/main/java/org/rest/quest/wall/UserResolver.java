package org.rest.quest.wall;

import org.rest.quest.ClientUtils;
import org.rest.quest.domain.User;
import org.rest.quest.service.UserService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * trying to overcome first wall (motto controller)
 */
public class UserResolver {

    private static final Logger logger = Logger.getLogger(UserResolver.class.getSimpleName());

    public String getKeyword(String hostUrl, String keyword, int teamId){
        List<User> users = new ArrayList<>();

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity entity = ClientUtils.getHttpHeadersEntity(keyword, teamId);

        HttpEntity<List> puzzleResponseEntity = restTemplate.exchange(
                hostUrl + "users", HttpMethod.GET, entity, List.class, Collections.emptyMap());

        logger.info("count of users: " + puzzleResponseEntity.getBody().size());
        logger.info("starting fetching tribe info");
        for(Object object : puzzleResponseEntity.getBody()){
            LinkedHashMap userMap = (LinkedHashMap) object;

            HttpEntity<User> response = restTemplate.exchange(
                    hostUrl + "users/" + userMap.get("name"), HttpMethod.GET, entity, User.class, Collections.emptyMap());

            users.add(new User(response.getBody().getName(),
                    response.getBody().getTribe(), response.getBody().getGender()));
        }
        logger.info("fetched.");
        Map<String, User> usersMap = users.stream().collect(Collectors.toMap(User::getName, user -> user));
        UserService userService = new UserService();
        logger.info("Calculating keyword.");
        return userService.calculateKeyWord(usersMap);
    }



}
