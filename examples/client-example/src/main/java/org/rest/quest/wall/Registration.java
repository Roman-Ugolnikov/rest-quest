package org.rest.quest.wall;

import org.rest.quest.domain.Team;
import org.rest.quest.domain.init.wrapper.MessageWrapper;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by roman on 19.03.16.
 */
public class Registration {

    public int registerTeam(String hostUrl, String teamName, String teamMotto){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Team team =new Team();
        team.setName(teamName);
        team.setMotto(teamMotto);

        MessageWrapper registrationMessageWrapper = restTemplate.postForObject(hostUrl + "/teams", team, MessageWrapper.class);
        //after words "Your team registered, id: "
        int teamId = Integer.parseInt(registrationMessageWrapper.getMessage().substring(26).trim());
        return teamId;
    }
}
