package org.rest.quest.wall;

import org.rest.quest.ClientUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.logging.Logger;

/**
 * trying to overcome first wall (motto controller)
 */
public class PuzzleResolver {

    public static final Logger logger = Logger.getLogger(PuzzleResolver.class.getSimpleName());

    public int getKeywordIndex(String hostUrl, String keyword, int teamId, int startIndex, int endIndex, String header) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity entity = ClientUtils.getHttpHeadersEntity(keyword, teamId);

        HttpEntity<String> puzzleResponseEntity = restTemplate.exchange(
                hostUrl + "puzzle/" + (startIndex + endIndex) / 2, HttpMethod.GET, entity, String.class, Collections.emptyMap());
        int resultOfComparison = Integer.parseInt(puzzleResponseEntity.getHeaders().get(header).get(0));
        if (resultOfComparison == 0) {
            return (startIndex + endIndex) / 2;
        }
        if (resultOfComparison > 0) {
            return getKeywordIndex(hostUrl, keyword, teamId, (startIndex + endIndex) / 2, endIndex, header);
        } else {
            return getKeywordIndex(hostUrl, keyword, teamId, startIndex, (startIndex + endIndex) / 2, header);
        }
    }


}
