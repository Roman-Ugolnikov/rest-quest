package org.rest.quest;

import org.rest.quest.wall.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.UnknownHostException;
import java.util.logging.Logger;

@SpringBootApplication
public class RestClientApplication {

    private static final Logger logger = Logger.getLogger(RestClientApplication.class.getSimpleName());
    private static String HOST_URL = "http://localhost:8080/";

    public static void main(String[] args) throws UnknownHostException {

        SpringApplication.run(RestClientApplication.class, args);
        if (System.getProperty("url") != null) {
            HOST_URL = System.getProperty("url");
            System.out.println("url received: " + System.getProperty("url"));
        }

        //registration
        int teamId = new Registration().registerTeam(HOST_URL, "automat", null);
        //1st wall
        String keyword = new MottoResolver().getKeyword(HOST_URL, null, teamId, 0, 221);
        logger.info("key word from MOTTO is: " + keyword);
        //2st wall
        int firstPuzzle = new PuzzleResolver().getKeywordIndex(HOST_URL, keyword, teamId, 0, 30000, "direction-one-to-go");
        int secondPuzzle = new PuzzleResolver().getKeywordIndex(HOST_URL, keyword, teamId, 0, 30000, "direction-two-to-go");
        logger.info("key word from PUZZLE id is : " + (firstPuzzle + secondPuzzle) / 2);
        keyword = "25";
        //3rd wall
        keyword = new UserResolver().getKeyword(HOST_URL, keyword, 0);
        logger.info("key word from USERS is: " + keyword);
        //4st wall
        WinnerResolver.sendWinnerUrl(HOST_URL, keyword, teamId, false);
        logger.info("we are winners");
    }

}
