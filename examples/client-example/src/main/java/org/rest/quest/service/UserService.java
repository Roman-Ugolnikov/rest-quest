package org.rest.quest.service;

import org.rest.quest.domain.User;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Roman Uholnikov
 */
public class UserService {

    public static final Logger logger = Logger.getLogger(UserService.class.getSimpleName());

    public static final int KEY_WORD_CHARACTER_COUNT = 4;


    public String calculateKeyWord(Map<String, User> users) {
        String keyword = "";
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        SortedMap<Character, CharacterWrapper> alphabetCount = new TreeMap<>();
        List<CharacterWrapper> alphabetSortedListByUsage;
        //adds whole alphabet
        for (int i = 0; i < chars.length; i++) {
            alphabetCount.put(chars[i], new CharacterWrapper(chars[i], 0l));
        }
        //calculates usage
        users.values().stream()
                .filter(user -> user.getGender().equalsIgnoreCase("female") && user.getTribe().equalsIgnoreCase("hobbits"))
                .forEach(filteredUser -> {
                    char[] nameChars = filteredUser.getName().toLowerCase().toCharArray();
                    for (int i = 0; i < nameChars.length; i++) {
                        alphabetCount.get(nameChars[i]).incrementUsage();
                    }
                });
        //sort according to usage
        alphabetSortedListByUsage = new ArrayList<>(alphabetCount.values());
        alphabetSortedListByUsage.sort(new CharacterWrapperComparator());

        //get first most used chars
        for (int i = 0; i < KEY_WORD_CHARACTER_COUNT; i++) {
            keyword += alphabetSortedListByUsage.get(i).getCharacter().toString();
        }
        return keyword;
    }


    class CharacterWrapper {
        Character character;
        Long usage;

        public Character getCharacter() {
            return character;
        }

        public void setCharacter(Character character) {
            this.character = character;
        }

        public Long getUsage() {
            return usage;
        }

        public void setUsage(Long usage) {
            this.usage = usage;
        }

        public void incrementUsage() {
            this.usage ++;
        }

        public CharacterWrapper(Character character, Long usage) {
            this.character = character;
            this.usage = usage;
        }
    }

    class CharacterWrapperComparator implements Comparator<CharacterWrapper>{

        @Override
        public int compare(CharacterWrapper o1, CharacterWrapper o2) {
            return o2.getUsage().compareTo(o1.getUsage());
        }
    }

}
