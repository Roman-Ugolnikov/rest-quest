package org.rest.quest.wall;

import org.rest.quest.ClientUtils;
import org.rest.quest.domain.MottoWrapper;
import org.rest.quest.domain.init.wrapper.MottoComparisonWrapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

/**
 * trying to overcome first wall (motto controller)
 */
public class MottoResolver {

    private static final Logger logger = Logger.getLogger(MottoResolver.class.getSimpleName());
    public static final int KEY_WORD_INDEX = 3;

    public String getKeyword(String hostUrl, String keyword, int teamId, int startIndex, int endIndex) {
        List<MottoWrapper> mottoWrappers = new ArrayList<>();

        for (int i = startIndex; i < endIndex; i++) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            HttpEntity entity = ClientUtils.getHttpHeadersEntity(keyword, teamId);

            HttpEntity<MottoWrapper> mottoWrapperResponseEntity = restTemplate.exchange(
                    hostUrl + "motto/" + i, HttpMethod.GET, entity, MottoWrapper.class, Collections.emptyMap());
            mottoWrappers.add(mottoWrapperResponseEntity.getBody());
        }

        mottoWrappers.sort(new MottoComparator(keyword, teamId, hostUrl));

        logger.info(mottoWrappers.get(3).getMotto());

        return mottoWrappers.get(KEY_WORD_INDEX).getMotto();
    }

    private class MottoComparator implements Comparator<MottoWrapper> {

        String keyword;
        String hostUrl;
        int teamId;

        MottoComparator(final String keyword, final int teamId, String hostUrl) {
            this.keyword = keyword;
            this.teamId = teamId;
            this.hostUrl = hostUrl;
        }

        @Override
        public int compare(final MottoWrapper o1, final MottoWrapper o2) {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add("team-id", String.valueOf(teamId));
            headers.add("Content-Type", "application/json");

            HttpEntity entity = new HttpEntity(new MottoComparisonWrapper(o1.getId(), o2.getId()), headers);

            HttpEntity<String> mottoWrapperResponseEntity = restTemplate.exchange(
                    hostUrl + "motto", HttpMethod.POST, entity, String.class, Collections.emptyMap());

            logger.info("comparing " + o1.getId() + " with " + o2.getId());

            return Integer.parseInt(mottoWrapperResponseEntity.getBody());
        }
    }


}
