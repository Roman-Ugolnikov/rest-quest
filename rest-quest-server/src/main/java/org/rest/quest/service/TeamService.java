package org.rest.quest.service;

import org.rest.quest.domain.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * @author Roman Uholnikov
 */
@Service
public class TeamService {

    public static final Logger logger = Logger.getLogger(TeamService.class.getSimpleName());

    public static final ConcurrentHashMap<Integer, Team> teams = new ConcurrentHashMap<>();

    @Autowired
    UserService userService;

    @Autowired
    MottoService mottoService;

    @PostConstruct
    private void init() {
        Team team;
        //just for demo purposes
        team = new Team();
        team.setId(teams.size());
        team.setName("demo-team");
        team.setMotto(mottoService.getRandomMotto());
        team.setMembers(new ArrayList<>());
        team.getMembers().add(userService.getRandomUser().getName());
        team.getMembers().add(userService.getRandomUser().getName());
        team.getMembers().add(userService.getRandomUser().getName());
        team.getMembers().add(userService.getRandomUser().getName());
        teams.put(team.getId(), team);
    }


    public Collection<Team> getAllTeams() {
        return teams.values();
    }

    public int addTeam(Team team) {
        if (team.getId() == null) {
            team.setId(teams.size());
        }
        teams.put(team.getId(), team);
        logger.info(MessageFormat.format("\n ************************* \n{0}: team registered: " +
                "\nname:{1} \nmotto:{2} \nmembers: {3}\n", teams.size(), team.getName(), team.getMotto(), team.getMembers()));
        return team.getId();
    }

    public Team getTeam(int id) {
        return teams.get(id);
    }

    public void logWinner(Team team) {
        System.out.println("****************************************");
        System.out.println("****************FINISHED****************");
        System.out.println("**********name: " + team.getName());
        System.out.println("*********motto: " + team.getMotto());
        System.out.println("*******members: " + team.getMembers());
        System.out.println("***finished at: " + team.getWinTime());
        System.out.println("*sent requests: " + team.getRequestsPerWall());
        System.out.println("****************************************");
//        logger.warning("this team is winner: " + team.toString());
    }

}
