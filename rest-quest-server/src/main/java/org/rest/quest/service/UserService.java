package org.rest.quest.service;

import com.google.gson.Gson;
import org.rest.quest.ServerUtils;
import org.rest.quest.domain.User;
import org.rest.quest.domain.init.wrapper.CreatureWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * @author Roman Uholnikov
 */
@Service
public class UserService {

    public static final Logger logger = Logger.getLogger(UserService.class.getSimpleName());

    public static final ConcurrentHashMap<String, User> users = new ConcurrentHashMap<>();
    public static final int KEY_WORD_CHARACTER_COUNT = 4;
    public static final int MAX_SIZE = 100;

    public String keyword = "";

    @PostConstruct
    private void init() {

        String string = ServerUtils.getStringFromFIle("init/names.json", UserService.class.getClassLoader());
        CreatureWrapper creatureWrapper = new Gson().fromJson(string, CreatureWrapper.class);

        creatureWrapper.hobbits.female.stream().limit(MAX_SIZE).forEach(name -> users.put(name, new User(name, "hobbits", "female")));
        creatureWrapper.people.female.stream().limit(MAX_SIZE).forEach(name -> users.put(name, new User(name, "people", "female")));
        creatureWrapper.hobbits.male.stream().limit(MAX_SIZE).forEach(name -> users.put(name, new User(name, "hobbits", "male")));
        creatureWrapper.people.male.stream().limit(MAX_SIZE).forEach(name -> users.put(name, new User(name, "people", "male")));

        logger.info(users.size() + " users loaded");
        calculateKeyWord();
        logger.info(getKeyword() + " is keyword");

    }

    private void calculateKeyWord() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        SortedMap<Character, CharacterWrapper> alphabetCount = new TreeMap<>();
        List<CharacterWrapper> alphabetSortedListByUsage;
        //adds whole alphabet
        for (int i = 0; i < chars.length; i++) {
            alphabetCount.put(chars[i], new CharacterWrapper(chars[i], 0l));
        }
        //calculates usage
        users.values().stream()
                .filter(user -> user.getGender().equalsIgnoreCase("female") && user.getTribe().equalsIgnoreCase("hobbits"))
                .forEach(filteredUser -> {
                    char[] nameChars = filteredUser.getName().toLowerCase().toCharArray();
                    for (int i = 0; i < nameChars.length; i++) {
                        alphabetCount.get(nameChars[i]).incrementUsage();
                    }
                });
        //sort according to usage
        alphabetSortedListByUsage = new ArrayList<>(alphabetCount.values());
        alphabetSortedListByUsage.sort(new CharacterWrapperComparator());

        //get first most used chars
        for (int i = 0; i < KEY_WORD_CHARACTER_COUNT; i++) {
            keyword += alphabetSortedListByUsage.get(i).getCharacter().toString();
        }
    }


    public User getUser(String name) {
        return users.get(name);
    }

    public Collection<User> getAllUsers() {
        return users.values();
    }

    public User getRandomUser() {
        return users.values().stream().findFirst().get();
    }

    public String getKeyword() {
        return keyword;
    }

    class CharacterWrapper {
        Character character;
        Long usage;

        public Character getCharacter() {
            return character;
        }

        public void setCharacter(Character character) {
            this.character = character;
        }

        public Long getUsage() {
            return usage;
        }

        public void setUsage(Long usage) {
            this.usage = usage;
        }

        public void incrementUsage() {
            this.usage++;
        }

        public CharacterWrapper(Character character, Long usage) {
            this.character = character;
            this.usage = usage;
        }
    }

    class CharacterWrapperComparator implements Comparator<CharacterWrapper> {

        @Override
        public int compare(CharacterWrapper o1, CharacterWrapper o2) {
            return o2.getUsage().compareTo(o1.getUsage());
        }
    }

}
