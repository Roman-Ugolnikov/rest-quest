package org.rest.quest.service;

import com.google.gson.Gson;
import org.rest.quest.ServerUtils;
import org.rest.quest.domain.init.wrapper.MottoWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Roman Uholnikov
 */
@Service
public class MottoService {

    public static final Logger logger = Logger.getLogger(MottoService.class.getSimpleName());
    public static final int KEY_MOTTO_INDEX = 3;

    private static int KEYWORD_MOTTO_ID;

    public static volatile String KEYWORD_MOTTO;

    public static List<MottoWrapper> motto = new ArrayList<>();

    @PostConstruct
    private void init() {
        String string = ServerUtils.getStringFromFIle("init/mottos.json", MottoService.class.getClassLoader());
        List<String> motto;
        motto = (List<String>) new Gson().fromJson(string, Map.class).get("motto");
        calculateKeyword(motto);

        logger.info(motto.size() + " motto registered");
        logger.info(KEYWORD_MOTTO + " keyword motto");
        logger.info(KEYWORD_MOTTO_ID + " keyword motto index");
    }

    public String getRandomMotto() {
        Random generator = new Random();
        int randomI = generator.nextInt(motto.size());
        return motto.get(randomI).getMotto();
    }

    public MottoWrapper getMotto(int id) {
        return motto.get(id);
    }

    public void calculateKeyword(List<String> motto) {
        Map<String, MottoWrapper> mottosWithWeights = new HashMap<>();
        //set id
        for (int i = 0; i < motto.size(); i++) {
            mottosWithWeights.put(motto.get(i), new MottoWrapper(i, motto.get(i), 0));
        }
        List<MottoWrapper> mottoSortedList = new ArrayList<>(mottosWithWeights.values());

        //sort
        Collections.shuffle(mottoSortedList);

        //set weight
        for (int i = 0; i < mottoSortedList.size(); i++) {
            mottoSortedList.get(i).setWeight(i);
        }
        //set result list
        MottoService.motto = mottoSortedList.stream()
                .sorted((m1, m2) -> m1.getId().compareTo(m2.getId())).collect(Collectors.toList());

        KEYWORD_MOTTO = mottoSortedList.get(KEY_MOTTO_INDEX).getMotto();
        KEYWORD_MOTTO_ID = mottoSortedList.get(KEY_MOTTO_INDEX).getId();
    }

}
