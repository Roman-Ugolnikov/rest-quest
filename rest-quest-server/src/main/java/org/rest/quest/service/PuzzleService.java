package org.rest.quest.service;

import com.google.gson.Gson;
import org.rest.quest.ServerUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

/**
 * @author Roman Uholnikov
 */
@Service
public class PuzzleService {

    public static final Logger logger = Logger.getLogger(PuzzleService.class.getSimpleName());

    public static Integer KEY_RECORD_1 = null;
    public static Integer KEY_RECORD_2 = null;
    public static Integer PUZZLE_COUNT = 30000;

    /**
     * each puzzle consist of 2 fields: 0 - puzzle, 1 - answer
     */
    public static List<List<String>> puzzle = new ArrayList<>();


    @PostConstruct
    private void init() {
        String string = ServerUtils.getStringFromFIle("init/puzzles.json", PuzzleService.class.getClassLoader());
        puzzle = (List<List<String>>) new Gson().fromJson(string, Map.class).get("puzzle");
        puzzle.addAll(puzzle);

        KEY_RECORD_1 = new Random().nextInt(PUZZLE_COUNT);
        KEY_RECORD_2 = new Random().nextInt(PUZZLE_COUNT);

        logger.info(puzzle.size() + " puzzles registered");
        logger.info(KEY_RECORD_1 + " first key record id");
        logger.info(KEY_RECORD_2 + " second key record id");
        logger.info((KEY_RECORD_1 + KEY_RECORD_2) / 2 + " puzzle record");
        logger.info(getKeywordPuzzle() + " keyword puzzle");
    }

    public String getRandomPuzzle() {
        Random generator = new Random();
        int randomI = generator.nextInt(puzzle.size());
        return puzzle.get(randomI).get(0);
    }

    public String getKeywordPuzzle() {

        return "Seller sells a hat. Is worth 10 $. A buyer approached, tried on the hat and agreed to buy, " +
                "but he had only 25 $ banknote. The seller sends his sun to change these 25 to a neighbor." +
                " Boy did it and brought back 10 + 10 + 5 banknote. The seller gave the hat and change of 15 $ " +
                "to the buyer. Buyer went away. " +
                "After a while the a neighbor came and said that the banknote of 25 $ is false and insisted to give his" +
                " money back. The Seller get 25$ from his cash desk and returned money.\n" +
                "\n" +
                "Question: how much money the Seller did loose?";
        //todo check spelling.
    }

    public String getKeywordPuzzleSolution() {
        //see resources/answer.png for more details
        return "25";
    }

}
