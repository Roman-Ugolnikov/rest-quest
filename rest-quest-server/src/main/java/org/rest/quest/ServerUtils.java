package org.rest.quest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.rest.quest.service.TeamService;

import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.*;
import java.util.Random;
import java.util.logging.Logger;

/**
 * @author Roman Uholnikov
 */
public class ServerUtils {

    private static final Logger logger = Logger.getLogger(ServerUtils.class.getSimpleName());

    /**
     * Get random string with specified length.
     *
     * @param length leang of string to be created.
     * @return word
     */
    public static String getRandomString(int length) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }


    public static String toJson(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    public static String toJson(Object obj, ExclusionStrategy strategy) {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(strategy).create();
        return gson.toJson(obj);
    }


    public static String getStringFromFIle(String fileName, ClassLoader classLoader) {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = null;
        try (InputStream in = classLoader.getResourceAsStream(fileName)) {
            reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            logger.warning(e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //do nothing
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Tries to play sound from resources.
     */
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("media/tada.wav").getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception ex) {
            try {
                String bip = "media/tada.mp3";
                Media hit = new Media(bip);
                MediaPlayer mediaPlayer = new MediaPlayer(hit);
                mediaPlayer.play();
            } catch (Exception e) {
                logger.warning("was not able to play sound" + e.getMessage());
            }
        }
    }

    public static void noteRequest(int wallNumber,  HttpServletRequest request, TeamService teamService){
        String teamIdString = request.getHeader("team-id");
        if(teamIdString != null) {
            teamService.getTeam(Integer.parseInt(teamIdString)).incrementRequestsCount(wallNumber);
        }
    }

}
