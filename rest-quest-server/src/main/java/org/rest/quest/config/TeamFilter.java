package org.rest.quest.config;

import org.rest.quest.domain.Team;
import org.rest.quest.service.TeamService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TeamFilter implements Filter {

    private TeamService teamService;

    public TeamFilter(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String teamId = httpServletRequest.getHeader("team-id");
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        try {
            Team team = teamService.getTeam(Integer.valueOf(teamId));
            if (team == null) {
                httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
                httpServletResponse.getWriter().append("You should specify request header 'team-id'=[your team id]. Check your team id /teams");
                httpServletResponse.flushBuffer();
                return;
            }
            chain.doFilter(request, response);
        } catch (NumberFormatException e) {
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            httpServletResponse.getWriter().append("wrong value for 'team-id' header");
            httpServletResponse.flushBuffer();
        }
    }

    @Override
    public void destroy() {

    }
}
