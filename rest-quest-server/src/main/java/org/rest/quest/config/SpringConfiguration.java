package org.rest.quest.config;


import org.rest.quest.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {

    @Autowired
    TeamService teamService;

    @Bean
    public FilterRegistrationBean someFilterRegistration() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new TeamFilter(teamService));
        registration.addUrlPatterns("/users/*", "/puzzle/*", "/motto/*", "/winners/registration");
        registration.setName("teamIdFilter");
        return registration;
    }
}
