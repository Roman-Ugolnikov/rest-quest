package org.rest.quest.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.rest.quest.ServerUtils;
import org.rest.quest.domain.User;
import org.rest.quest.domain.UserFieldExclusionStrategy;
import org.rest.quest.domain.init.wrapper.MessageWrapper;
import org.rest.quest.service.PuzzleService;
import org.rest.quest.service.TeamService;
import org.rest.quest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Roman Uholnikov
 */
@RestController
public class UserController {

    @Autowired
    PuzzleService puzzleService;

    @Autowired
    UserService userService;

    @Autowired
    TeamService teamService;

    @HystrixCommand(commandKey = "3st_wall")
    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json")
    public String users(HttpServletRequest request, HttpServletResponse response) {
        ServerUtils.noteRequest(3, request, teamService);
        if (request.getHeader("keyword") == null || !request.getHeader("keyword").equalsIgnoreCase(puzzleService.getKeywordPuzzleSolution())) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return new MessageWrapper("you must enter valid 'keyword' header with value calculated on the previous step").toString();
        }
        return ServerUtils.toJson(userService.getAllUsers(), new UserFieldExclusionStrategy("tribe"));
    }

    @HystrixCommand(commandKey = "3st_wall")
    @RequestMapping(value = "/users/{userName}", method = RequestMethod.GET, produces = "application/json")
    public User user(@PathVariable String userName, HttpServletRequest request, HttpServletResponse response) {
        ServerUtils.noteRequest(3, request, teamService);
        return userService.getUser(userName);
    }

}
