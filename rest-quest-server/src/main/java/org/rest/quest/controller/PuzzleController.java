package org.rest.quest.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.rest.quest.ServerUtils;
import org.rest.quest.domain.init.wrapper.MessageWrapper;
import org.rest.quest.service.MottoService;
import org.rest.quest.service.PuzzleService;
import org.rest.quest.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Roman Uholnikov
 */
@RestController
public class PuzzleController {

    @Autowired
    PuzzleService puzzleService;

    @Autowired
    TeamService teamService;

    @RequestMapping(value = "/puzzle", method = RequestMethod.GET, produces = "application/json")
    public MessageWrapper puzzles() {
        return new MessageWrapper("Имеем 30 000 загадок (/puzzle/0..30000). Отгадка ключевой загадки является keyword" +
                " для следующей стены (отгадка — английское слово в нижнем регистре). Ключевая загадка " +
                "находится равноудаленной от двух других, на которые указывают заголовки direction-one-to-go " +
                "и direction-two-to-go в ответах сервера (server response). \n" +
                "Например: номера загадок 6 и 10. Равноудаленной является загадка 8.  ( 6 7 8 9 10 ).\n" +
                "Если  равноудаленность подсчитать невозможно (к примеру 6 и 9) то округление происходит в мешьшую " +
                "сторону (в примере с 6 и 9 равноудаленным будет считаться 7).\n");
    }

    @HystrixCommand(commandKey = "2rd_wall")
    @RequestMapping(value = "/puzzle/{id}", method = RequestMethod.GET, produces = "application/json")
    public MessageWrapper getPuzzle(@PathVariable int id, HttpServletRequest request, HttpServletResponse response) {
        ServerUtils.noteRequest(2, request, teamService);
        if (request.getHeader("keyword") == null || !request.getHeader("keyword").equalsIgnoreCase(MottoService.KEYWORD_MOTTO)) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return new MessageWrapper("you must enter valid 'keyword' header with value calculated on the previous step");
        }

        response.setHeader("direction-one-to-go", String.valueOf(PuzzleService.KEY_RECORD_1.compareTo(id)));
        response.setHeader("direction-two-to-go", String.valueOf(PuzzleService.KEY_RECORD_2.compareTo(id)));

        if (id == (PuzzleService.KEY_RECORD_1 + PuzzleService.KEY_RECORD_2) / 2) {
            return new MessageWrapper("Here is your keyword puzzle", puzzleService.getKeywordPuzzle());
        } else {
            return new MessageWrapper(puzzleService.getRandomPuzzle());
        }
    }

}
