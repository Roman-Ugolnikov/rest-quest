package org.rest.quest.controller;

import org.rest.quest.domain.Team;
import org.rest.quest.domain.init.wrapper.MessageWrapper;
import org.rest.quest.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author Roman Uholnikov
 */
@RestController
public class TeamController {

    @Autowired
    TeamService teamService;

    @RequestMapping(value = "/teams", method = RequestMethod.GET, produces = "application/json")
    public Collection<Team> teamsGet() {
        return teamService.getAllTeams();
    }

    @RequestMapping(value = "/teams", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public MessageWrapper addTeam(@RequestBody Team team) {
        int teamId = teamService.addTeam(team);
        teamService.getTeam(teamId).incrementRequestsCount(0);
        return new MessageWrapper("Your team registered, id: " + teamId);
    }

}
