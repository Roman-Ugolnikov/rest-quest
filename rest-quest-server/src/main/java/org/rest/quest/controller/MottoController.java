package org.rest.quest.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.rest.quest.ServerUtils;
import org.rest.quest.domain.init.wrapper.MessageWrapper;
import org.rest.quest.domain.init.wrapper.MottoComparisonWrapper;
import org.rest.quest.domain.init.wrapper.MottoWrapper;
import org.rest.quest.service.MottoService;
import org.rest.quest.service.TeamService;
import org.rest.quest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Roman Uholnikov
 */
@RestController
public class MottoController {

    @Autowired
    UserService userService;

    @Autowired
    MottoService mottoService;

    @Autowired
    TeamService teamService;

    @RequestMapping(value = "/motto", method = RequestMethod.GET, produces = "application/json")
    public MessageWrapper mottoGet() {
        return new MessageWrapper("You have a lot of motto. It is available under url /{id}.");
    }

    @HystrixCommand(commandKey = "1nd_wall")
    @RequestMapping(value = "/motto", method = RequestMethod.POST, produces = "application/json")
    public String motto( HttpServletRequest request, @RequestBody MottoComparisonWrapper mottoComparisonWrapper) throws IOException {
        ServerUtils.noteRequest(1, request, teamService);

        MottoWrapper m1 = mottoService.getMotto(mottoComparisonWrapper.motto1);
        MottoWrapper m2 = mottoService.getMotto(mottoComparisonWrapper.motto2);
        return String.valueOf(m1.getWeight().compareTo(m2.getWeight()));
    }

    @HystrixCommand(commandKey = "1nd_wall")
    @RequestMapping(value = "/motto/{id}", method = RequestMethod.GET, produces = "application/json")
    public MottoWrapper motto(@PathVariable Integer id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServerUtils.noteRequest(1, request, teamService);

        return mottoService.getMotto(id);
    }

}
