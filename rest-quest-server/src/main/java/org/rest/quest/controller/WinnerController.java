package org.rest.quest.controller;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.rest.quest.ServerUtils;
import org.rest.quest.domain.Team;
import org.rest.quest.domain.init.wrapper.MessageWrapper;
import org.rest.quest.domain.init.wrapper.RequestWrapper;
import org.rest.quest.service.TeamService;
import org.rest.quest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.MessageFormat;
import java.time.LocalTime;
import java.util.stream.Collectors;

@RestController
public class WinnerController {

    public static final int TIME_KEYWORD_LIVES = 3000;
    private volatile String keyword;

    @Autowired
    TeamService teamService;

    @Autowired
    UserService userService;

    @HystrixCommand(commandKey = "4rd_wall")
    @RequestMapping(value = "/winners", method = RequestMethod.POST, produces = "application/json")
    public synchronized MessageWrapper addRequest(@RequestBody RequestWrapper requestWrapper,
                                                  HttpServletRequest request, HttpServletResponse response) {
        ServerUtils.noteRequest(4, request, teamService);
        if (checkKey(request, response, "keyword", userService.getKeyword())) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return new MessageWrapper("you must enter valid 'keyword' header with value calculated on the previous step");
        }
        keyword = ServerUtils.getRandomString(8);
        startTimerForKeyWord();
        teamService.getTeam(Integer.parseInt(request.getHeader("team-id"))).setServerUrl(requestWrapper.getTeamUrl());
        startTimerForSendingLastKeyword(keyword, requestWrapper.getTeamUrl());
        response.setStatus(HttpServletResponse.SC_CREATED);
        return new MessageWrapper("we will send you keyword." +
                " It will live (if no one regenerates it) for " + TIME_KEYWORD_LIVES / 1000);
    }

    @HystrixCommand(commandKey = "4rd_wall")
    @RequestMapping(value = "/winners/registration", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public MessageWrapper addRequestApprove(HttpServletRequest request, HttpServletResponse response) {
        ServerUtils.noteRequest(4, request, teamService);
        if (checkKey(request, response, "keyword", keyword)) {
            return new MessageWrapper("you must enter valid 'keyword' header with value calculated on the previous step. " +
                    "Remember it lives " + TIME_KEYWORD_LIVES / 1000 + " seconds!");
        }

        Team team = teamService.getTeam(Integer.parseInt(request.getHeader("team-id")));
        team.setWinTime(LocalTime.now());
        teamService.logWinner(team);
        ServerUtils.playSound();
        return new MessageWrapper("SUCCESS. You finished quest!");
    }


    @ResponseBody
    @RequestMapping(value = "/winners", method = RequestMethod.GET, produces = "application/json")
    public String winnersTable() {
        return ServerUtils.toJson(teamService.getAllTeams()
                .stream()
                .filter(team -> team.getServerUrl() != null && team.getWinTime() != null)
                .sorted((o1, o2) -> o1.getWinTime().compareTo(o2.getWinTime()))
                .collect(Collectors.toList()));
    }


    private void startTimerForKeyWord() {
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(TIME_KEYWORD_LIVES);
                } catch (InterruptedException e) {
                    //nothing to do
                } finally {
                    keyword = null;
                }
            }
        }.start();
    }

    private void startTimerForSendingLastKeyword(String keyword, String requestWrapper) {
        new Thread() {
            public void run() {

                try {
                    MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
                    headers.add("Content-Type", "application/json");

                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                    HttpEntity<MessageWrapper> request =
                            new HttpEntity<>(new MessageWrapper("Key will live for 3 seconds, do not hasitate", keyword), headers);

                    System.out.println(MessageFormat.format("************ Trying to send POST request to {0}.", requestWrapper));
                    ResponseEntity<String> responseEntity = restTemplate.postForEntity(requestWrapper, request, String.class);
                    System.out.println(MessageFormat.format("************ Response: {0}, message: {1}", responseEntity.getStatusCode(), responseEntity.getBody()));

                } catch (Exception ex) {
                    System.out.println(MessageFormat.format("************ Fail to send POST request to {0}. Exception: {1}",
                            requestWrapper, ex.getMessage()));
                }
            }
        }.start();
    }

    private boolean checkKey(HttpServletRequest request, HttpServletResponse response, String header, String keyword) {
        if (request.getHeader(header) == null || !request.getHeader(header).equalsIgnoreCase(keyword)) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return true;
        }
        return false;
    }

}
