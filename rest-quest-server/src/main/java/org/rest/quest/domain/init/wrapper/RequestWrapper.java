package org.rest.quest.domain.init.wrapper;

public class RequestWrapper {

    private String teamUrl;

    public RequestWrapper() {
    }

    public RequestWrapper(String teamUrl) {
        this.teamUrl = teamUrl;
    }

    public String getTeamUrl() {
        return teamUrl;
    }

    public void setTeamUrl(String teamUrl) {
        this.teamUrl = teamUrl;
    }
}
