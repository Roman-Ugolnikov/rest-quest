package org.rest.quest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @author Roman Uholnikov
 */
public class Team {

    private Integer id;

    private String name;

    private String motto;

    private List<String> members;

    @JsonIgnore
    AtomicIntegerArray requestsPerWall = new AtomicIntegerArray(5);

    @JsonIgnore
    private String serverUrl;

    @JsonIgnore
    private LocalTime winTime;

    public Team() {
    }


    /**
     * Increment amount of sent requests for particular wall number.
     *
     * @param wallNumber - number of wall. Starts from 1 (to 4). 0 is registration.
     */
    public void incrementRequestsCount(int wallNumber){
        requestsPerWall.incrementAndGet(wallNumber);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public LocalTime getWinTime() {
        return winTime;
    }

    public void setWinTime(LocalTime winTime) {
        this.winTime = winTime;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public AtomicIntegerArray getRequestsPerWall() {
        return requestsPerWall;
    }


    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", motto='" + motto + '\'' +
                ", members=" + members +
                ", requestsPerWall=" + requestsPerWall +
                ", serverUrl='" + serverUrl + '\'' +
                ", winTime=" + winTime +
                '}';
    }
}
