package org.rest.quest.domain.init.wrapper;

import java.util.List;

/**
 * @author Roman Uholnikov
 */
public class People {
    public List<String> male;
    public List<String> female;

}
