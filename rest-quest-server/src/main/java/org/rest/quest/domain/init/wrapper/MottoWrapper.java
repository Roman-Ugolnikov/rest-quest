package org.rest.quest.domain.init.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Roman Uholnikov
 */
public class MottoWrapper {
    int id;
    String motto;
    @JsonIgnore
    int weight;

    public MottoWrapper(final int id, final String motto, final int weight) {
        this.id = id;
        this.motto = motto;
        this.weight = weight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(final String motto) {
        this.motto = motto;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(final int weight) {
        this.weight = weight;
    }

}
