package org.rest.quest.domain;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * @author Roman Uholnikov
 */
public class UserFieldExclusionStrategy implements ExclusionStrategy {

    String fieldToExclude;

    public UserFieldExclusionStrategy() {
    }

    public UserFieldExclusionStrategy(String fieldToExclude) {
        this.fieldToExclude = fieldToExclude;
    }

    public boolean shouldSkipClass(Class<?> arg0) {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {

        return (f.getDeclaringClass() == User.class && f.getName().equals(fieldToExclude));
    }
}
