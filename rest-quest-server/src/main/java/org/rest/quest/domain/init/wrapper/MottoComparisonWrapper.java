package org.rest.quest.domain.init.wrapper;

/**
 * @author Roman Uholnikov
 */
public class MottoComparisonWrapper {
    public int motto1;
    public int motto2;
}
